/*
	Описание: UDP -> TCP ретранслятор
	Автор: Драп Антон
	To link: gcc udptotcp.c -lpthread -o udptotcp
*/
 
#include <stdio.h>
#include <string.h>    
#include <stdlib.h>    
#include <arpa/inet.h> 
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h> 
#include <errno.h>
#include <mqueue.h>
#include <stdbool.h> 

#define MODULENAME "[udptotcp]: "

/* Уровень отладочных сообщений:
	0 - тихий режим, без сообщений
	1 - стандартные сообщения + ошибки
	2 - трассировка всех событий */
static int debugLevel = 1;

#define DEBUG_PRINT( level, frmStr, ... )\
	do{\
		if( (debugLevel) >= (level) )\
		{\
			printf( (frmStr), __VA_ARGS__ );\
		}\
	}while( 0 )

#define ERROR_PRINT( frmStr, ... ) DEBUG_PRINT( 1, frmStr, __VA_ARGS__ )

/* размер очереди входящих запросов UDP сервера */ 
#define INCOMMING_CONNECTION_QUEUE_SIZE 1

/* максимальный размер сообщения */
#define MAX_MSG_SIZE 120
/* размер префикса */	
#define PREFIX_SIZE 4


/* имя очережи данных */
#define DATA_QUEUE_NAME "/dataQueue1"
#define DATA_QUEUE_LENGTH 1000
#define DATA_QUEUE_MSG_SIZE MAX_MSG_SIZE	

/* порт UDP сервера по умолчанию  */
#define DEFAULT_LISTEN_PORT 8888

/* максимальная длина строки адреса */
#define MAX_ADDR_SIZE 512


/* Контекст приемника */
typedef struct tRxCntx
{
	struct sockaddr_in socketAddr;
	struct sockaddr_in srcAddr;
	mqd_t dataQueue;
	sem_t* isTcpConnectSem;

}tRxCntx;

/* Контекст передатчика */
typedef struct tTxCntx
{
	struct sockaddr_in socketAddr;
	struct sockaddr_in dstAddr;
	mqd_t dataQueue;
	sem_t* isTcpConnectSem;

	/* буфер для передачи данных, в начале буфера помещается префикс PREFIX_SIZE байт*/
	char buf[MAX_MSG_SIZE + PREFIX_SIZE];

}tTxCntx;


typedef void* (*tExecuteStateProc)( int* sock, tTxCntx* txCntx );

/*!
	\brief Потоковая процедура приемника данных
	\param data - внешние данные
*/
static void* rxThreadProc( void *data );

/*!
	\brief Потоковая процедура отправителя данных
	\param data - внешние данные
*/
static void* txThreadProc( void *data );

/*
	\brief Обработчик состояния передачи данных
*/
static void* transmitData( int* sock, tTxCntx* txCntx );

/*
	\brief Обработчик состояния подключения к серверу
*/
static void* connecting( int* sock, tTxCntx* txCntx );

/*!
	\brief Потоковая процедура отправителя данных
	\param headMsg - сообщение заголовка данных
	\param data - данные
	\param dataSize - размер данных
*/
static void printData( const char* headMsg, const char* data, int dataSize );

/*!
	\brief Печать информационного сообщения
*/
static void printInfo( void );

/*!
	\brief Проверка соединения
	\param sock - сокет
*/
static bool checkConnection( int* sock );

int main(int argc , char *argv[])
{
	int err;
	tRxCntx* rxCntx = NULL;
	tTxCntx* txCntx = NULL;
	mqd_t dataQueue = -1;
	sem_t isTcpConnectSem;

	pthread_t receiverThreadId;
	pthread_t senderThreadId;

	char prefix[PREFIX_SIZE] = { 0 };

	uint16_t listenUdpPort = DEFAULT_LISTEN_PORT;
	char listenUdpIf[MAX_ADDR_SIZE] = { 0 };

	char srcIpAddr[MAX_ADDR_SIZE] = { 0 };
	uint16_t srcIpPort = 0;

	char dstIpAddr[MAX_ADDR_SIZE] = { 0 };
	uint16_t dstIpPort = 0;

	int opt; 

	DEBUG_PRINT( 1, MODULENAME"%s\n", "Start..." );

	while( ( opt = getopt( argc, argv, "l:p:i:v" ) ) != -1 )  
	{
		switch( opt )  
		{
			case 'l':
				listenUdpPort = atol( optarg );
				DEBUG_PRINT( 2, MODULENAME"Listen udp port %d\n", listenUdpPort );
			break;
			case 'p':
				strncpy( prefix, optarg, PREFIX_SIZE );
				DEBUG_PRINT( 2, MODULENAME"Prefix '%s'\n", prefix );
			break;
			case 'i':
				strncpy( listenUdpIf, optarg, MAX_ADDR_SIZE );
				DEBUG_PRINT( 2, MODULENAME"Listen udp interface '%s'\n", listenUdpIf );
			break;
			case 'v':
				debugLevel = 2;
				DEBUG_PRINT( 2, MODULENAME"%s\n", "Verbose output enabled" );
			break;
			case '?':
				printInfo( );
				exit( 1 );
			break;
		}
	}

	DEBUG_PRINT( 2, MODULENAME"%s\n", "Checking parameters..." );
	if( argc - optind < 4 )
	{
		ERROR_PRINT( MODULENAME"%s\n\n", "Incorrect parameters" );
		printInfo( );
		exit( 1 );
	}

	for( int i = optind; i < argc; i++ )
	{
		switch( i - optind )
		{	
			case 0:
				strncpy( srcIpAddr, argv[i], MAX_ADDR_SIZE );
				DEBUG_PRINT( 2, MODULENAME"UDP source address '%s'\n", srcIpAddr );
			break;
			case 1:
				srcIpPort = atol( argv[i] );
				DEBUG_PRINT( 2, MODULENAME"UDP source port %d\n", srcIpPort );
			break;
			case 2:
				strncpy( dstIpAddr, argv[i], MAX_ADDR_SIZE );
				DEBUG_PRINT( 2, MODULENAME"Out TCP address '%s'\n", dstIpAddr );
			break;
			case 3:
				dstIpPort = atol( argv[i] );
				DEBUG_PRINT( 2, MODULENAME"Out TCP port %d\n", dstIpPort );
			break;
		}
	}    

	DEBUG_PRINT( 2, MODULENAME"%s\n", "Preparing..." );
	rxCntx = malloc( sizeof(*rxCntx) );
	if( !rxCntx )
	{
		ERROR_PRINT( MODULENAME"Alllocate memory to receiver context error %d\n", errno );
		exit( 1 );
	}
	txCntx = malloc( sizeof(*txCntx) );
	if( !txCntx )
	{
		ERROR_PRINT( MODULENAME"Alllocate memory to receiver context error %d\n", errno );
		exit( 1 );
	}

    //TBD Определиться c размером очереди! 
    //    М/б нужно регулировать размер очереди?
    int openFlags = O_RDWR | O_CREAT | O_EXCL;
    int permissions = S_IWUSR | S_IRUSR;
    struct mq_attr attr = (struct mq_attr){
	    	.mq_flags = 0,
            .mq_msgsize = DATA_QUEUE_MSG_SIZE,
            .mq_maxmsg = DATA_QUEUE_LENGTH,
            .mq_curmsgs = 0,
    };
	DEBUG_PRINT( 2, MODULENAME"%s\n", "Create queue..." );
	mq_unlink( DATA_QUEUE_NAME );
	dataQueue = mq_open( DATA_QUEUE_NAME, openFlags, permissions, &attr );
	if( dataQueue == (mqd_t) -1 )
	{
		ERROR_PRINT( MODULENAME"Data queue create error %d\n", errno );
		exit( 1 );
	}

	if( sem_init( &isTcpConnectSem, 0, 1 ) == -1 )
	{
		ERROR_PRINT( MODULENAME"Create semaphore error %d\n", errno );
		exit( 1 );
	}

	DEBUG_PRINT( 2, MODULENAME"%s\n", "Creating sender thread..." );
	txCntx->dataQueue = dataQueue;	
	txCntx->isTcpConnectSem = &isTcpConnectSem;
	txCntx->socketAddr.sin_family = AF_INET;
	inet_aton( listenUdpIf, &(txCntx->socketAddr.sin_addr) );
	txCntx->socketAddr.sin_port = 0;
	txCntx->dstAddr.sin_family = AF_INET;
	inet_aton( dstIpAddr, &(txCntx->dstAddr.sin_addr) );
	txCntx->dstAddr.sin_port = htons( dstIpPort );
	memcpy( txCntx->buf, prefix, PREFIX_SIZE );
	err = pthread_create( &senderThreadId, NULL, &txThreadProc, txCntx );
	if( err < 0 )
	{
		ERROR_PRINT( MODULENAME"Create UDP thread error %d\n", errno );
		exit( 1 );
	}
	
	DEBUG_PRINT( 2, MODULENAME"%s\n", "Creating receiver thread..." );
	rxCntx->dataQueue = dataQueue;	
	rxCntx->isTcpConnectSem = &isTcpConnectSem;
	rxCntx->socketAddr.sin_family = AF_INET;
	rxCntx->socketAddr.sin_addr.s_addr = INADDR_ANY;
	rxCntx->socketAddr.sin_port = htons( listenUdpPort );
	rxCntx->srcAddr.sin_family = AF_INET;
	inet_aton( srcIpAddr, &(rxCntx->srcAddr.sin_addr) );
	rxCntx->srcAddr.sin_port = htons( srcIpPort );
	err = pthread_create( &receiverThreadId, NULL, &rxThreadProc, rxCntx );
	if( err < 0 )
	{
		ERROR_PRINT( MODULENAME"Create UDP thread error %d\n", errno );
		exit( 1 );
	}

 
	pthread_join( receiverThreadId, NULL);
	pthread_join( senderThreadId, NULL);

	DEBUG_PRINT( 2, MODULENAME"%s\n", "RX and TX closed" );

	free( rxCntx );
	free( txCntx );
	if( dataQueue != (mqd_t) -1 )
	{
		mq_close( dataQueue );
	}
	mq_unlink( DATA_QUEUE_NAME );
	sem_destroy( &isTcpConnectSem );

	DEBUG_PRINT( 1, MODULENAME"%s\n", "Finshed" );

	return 0;
}
 
/*!
	\brief Потоковая процедура приемника данных
	\param data - внешние данные
*/
static void* rxThreadProc( void *data )
{
	tRxCntx* rxCntx = (tRxCntx*) data;
	int sock;
	char buf[MAX_MSG_SIZE] = {0};

	sock = socket( AF_INET, SOCK_DGRAM, 0 );
	if( sock == -1 )
	{
		ERROR_PRINT( MODULENAME"[UDP RX] Create socket for UDP server error %d\n", errno );
		goto ENDFUNC;
	}
	DEBUG_PRINT( 2, MODULENAME"%s\n", "[UDP RX] Udp socket is created successfully." );

	if( bind( sock, (struct sockaddr*) &rxCntx->socketAddr, sizeof( rxCntx->socketAddr ) ) < 0 )
	{
		ERROR_PRINT( MODULENAME"[UDP RX] Bind UDP socket error %d", errno );
		goto ENDFUNC;
	}
	DEBUG_PRINT( 2, MODULENAME"%s\n", "[UDP RX] Udp socket is binded successfully." );

	if( connect( sock, (struct sockaddr *)&rxCntx->srcAddr, sizeof( rxCntx->srcAddr ) ) < 0 )
	{
		ERROR_PRINT( MODULENAME"[UDP RX] Connect for UDP socket error %d\n", errno );
		goto ENDFUNC;
	}


	DEBUG_PRINT( 1, MODULENAME"%s\n", "[UDP RX] Ready to get data from UDP." );
	while( 1 )
	{
		int len = recvfrom( sock, buf, MAX_MSG_SIZE, 0, NULL, 0 );
		if( len > 0 )
		{
			int isTcpConnected = 0; 

			printData( "[UDP RX]", buf, len );

			sem_getvalue( rxCntx->isTcpConnectSem, &isTcpConnected );
			if( !isTcpConnected )
			{
				DEBUG_PRINT( 1, MODULENAME"%s\n", "[UDP RX] No TCP connection, data is skipped." );
			}
			else if( mq_send( rxCntx->dataQueue, buf, len, 0 ) == -1 )
			{
				ERROR_PRINT( MODULENAME"[UDP RX] Data queue overload, last received message is skipped. Error %d\n", errno );
			}
		}
	}

ENDFUNC:
	if( sock != -1 )
	{
		close( sock );
	}

	DEBUG_PRINT( 2, MODULENAME"%s\n", "[UDP RX] Receiving thread finished" );
	return NULL;
} 


/*!
	\brief Потоковая процедура отправителя данных
	\param data - внешние данные
*/
static void* txThreadProc( void *data )
{	
	tTxCntx* txCntx = (tTxCntx*) data;
	int sock = -1;
	
	tExecuteStateProc executeStateProc = &connecting;
	while( executeStateProc )
	{
		executeStateProc = executeStateProc( &sock, txCntx );
	}
	
	close( sock );
	DEBUG_PRINT( 2, MODULENAME"%s\n", "[TCP TX] Receiving thread finished" );
	return NULL;
}

/*
	\brief Обработчик состояния передачи данных
*/
static void* transmitData( int* sock, tTxCntx* txCntx )
{
	tExecuteStateProc nextState = &transmitData;

	int len = mq_receive( txCntx->dataQueue, txCntx->buf + PREFIX_SIZE, MAX_MSG_SIZE, NULL );
	if( len > 0 )
	{
		len += PREFIX_SIZE;

		if( !checkConnection( sock ) )
		{
		    nextState = &connecting;
		    goto ENDFUNC;
		}
		else if( send( *sock, txCntx->buf, len, 0 ) < 0 )
		{
			DEBUG_PRINT( 1, MODULENAME"[TCP TX] Send error %d\n", errno );
			checkConnection( sock );
			nextState = &connecting;			
		}
		else
		{
			printData( "[TCP TX]", txCntx->buf, len );
		}

		if( !checkConnection( sock ) )
		{
		    nextState = &connecting;
		    goto ENDFUNC;			
		}
	}
	else
	{
		ERROR_PRINT( MODULENAME"[TCP TX] Receive data from queue error %d\n", errno );
		nextState = connecting;
	}

ENDFUNC:
	return nextState;
}

/*
	\brief Обработчик состояния подключения к серверу
*/
static void* connecting( int* sock, tTxCntx* txCntx )
{
	sem_wait( txCntx->isTcpConnectSem );
	DEBUG_PRINT( 1, MODULENAME"%s\n", "[TCP TX] Try connect to TCP server..." );

	*sock = socket( AF_INET, SOCK_STREAM, 0 );
	if( *sock == -1 )
	{
		ERROR_PRINT( MODULENAME"[TCP TX] Create socket for TCP server error %d\n", errno );
		return NULL;
	}
	shutdown( *sock, 0 );
	DEBUG_PRINT( 2, MODULENAME"%s\n", "[TCP TX] TCP socket is created successfully." );


	while( 1 )
	{
		if( connect( *sock, (struct sockaddr *)&txCntx->dstAddr, sizeof(txCntx->dstAddr) ) == 0 )
		{
			break;
		}
		else
		{
			DEBUG_PRINT( 3, MODULENAME"[TCP TX] Connect error %d\n", errno );
		}
	}

	sem_post( txCntx->isTcpConnectSem );
	DEBUG_PRINT( 1, MODULENAME"%s\n", "[TCP TX] Connect to TCP server successfully" );

	return &transmitData;
}

/*!
	\brief Потоковая процедура отправителя данных
	\param headMsg - сообщение заголовка данных
	\param data - данные
	\param dataSize - размер данных
*/
static void printData( const char* headMsg, const char* data, int dataSize )
{
	DEBUG_PRINT( 1, MODULENAME"%s (%d): ", headMsg, dataSize );

	for( int i = 0; i < dataSize; ++i )
	{
		DEBUG_PRINT( 1, "%02X ", data[i] );
	}

	DEBUG_PRINT( 1, "%s", "\n" );
}

/*!
	\brief Печать информационного сообщения
*/
static void printInfo( void )
{
	DEBUG_PRINT( 1, "%s\n", "udptotcp is utility to retranslate data from udp to tcp" );
	DEBUG_PRINT( 1, "%s\n", "Using: udptotcp -i [interface] -l [port] -p [prefix] "
	                        "[src address] [source port] [destination address] "
	                        "[destination port]" );
	DEBUG_PRINT( 1, "%s\n", "Parameters:" );
	DEBUG_PRINT( 1, "%s\n", "	-i : listen UDP interface (any by default) " );
	DEBUG_PRINT( 1, "%s\n", "	-l : listen UDP port (8888 by default) " );
	DEBUG_PRINT( 1, "%s\n", "	-p : prefix to add to data that sended by TCP ('00 00 00 00' by default) " );
}

/*!
	\brief Проверка соединения
	\param sock - сокет
*/
static bool checkConnection( int* sock )
{	
	bool checkConnectionFl;
	int error = 0;
	socklen_t size = sizeof( error );
	int retval = getsockopt( *sock, SOL_SOCKET, SO_ERROR, &error, &size );

	checkConnectionFl = ( retval == 0 && error == 0 );
	if( !checkConnectionFl )
	{
		DEBUG_PRINT( 1, MODULENAME"[TCP TX] Connection to TCP server broke %d\n", errno );
		close( *sock );
		*sock = -1;
	}

	return checkConnectionFl;
}
